unit uOrmModel;

interface
uses
   System.SysUtils
   , mormot.core.base
   , mormot.orm.base
   , mormot.orm.core
   , mormot.rest.core
;
const
  MASTER_IP = '127.0.0.1';
  MASTER_PORT = '8206';
  REPL_PORT = '8207';
type
  TOrmUser = class(TOrm)
    private
      fUserID: RawUTF8;
      fName: RawUTF8;
      fVersion: TRecordVersion;
    published
      property UserID: RawUTF8 read fUserID write fUserID stored AS_UNIQUE;
      property Name: RawUTF8 read fName write fName;
      property version: TRecordVersion read fVersion write fVersion;
  end;
  TOrmUserObjArray = array of TOrmUser;

  function createSlaveModel: TOrmModel;
  function createMasterModel: TOrmModel;
  function createReplicationModel: TOrmModel;
implementation

function createSlaveModel: TOrmModel;
begin
  result := TOrmModel.Create([TOrmTableDeleted,TOrmUser], 'slave');
end;

function createMasterModel: TOrmModel;
begin
  result := TOrmModel.Create([TOrmTableDeleted,TOrmUser], 'master');
end;

function createReplicationModel: TOrmModel;
begin
  result := TOrmModel.Create([TOrmTableDeleted,TOrmUser], 'masterrepl');
end;
end.
