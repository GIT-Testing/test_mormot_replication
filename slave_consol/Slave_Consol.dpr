program Slave_Consol;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  mormot.db.raw.sqlite3,
  mormot.rest.sqlite3,
  mormot.core.log,
  mormot.core.base,
  mormot.soa.core,
  mormot.orm.core,
  mormot.rest.http.client,
  uOrmModel in '..\uOrmModel.pas',
  ReplicationTestInterfaces in '..\ReplicationTestInterfaces.pas',
  uSlaveServer in 'uSlaveServer.pas';

type
  TReplicationClass = class
  private
    { Private declarations }
    fSlave: TRestServerDB;                     // Slave rest server used in sync
    flocalServer: TRestServerDB;               // Slaves local rest server
    fService: IReplicationInterface;           // Service from master
    fRemoteClient: TRestHttpClient;            // Client in slavesystem using services at master
    fMasterClient: TRestHttpClientWebsockets;  // client used in sync
  public
  constructor create;
    { Public declarations }
  end;

var
  ILog: ISynLog;
  LogFamily: TSynLogFamily;
  createOK: boolean;
  Replication: TReplicationClass;

procedure writeMessage(pmcMsg: RawUTF8);
begin
  raise Exception.Create(pmcMsg);
end;

{ TReplicationClass }

constructor TReplicationClass.create;
var
  model: TOrmModel;
begin
  createOK := false;
  inherited create;
  ILog := TSynlog.Enter('TReplicationClass.Create', []);
  ILog.Log(sllInfo, 'TestReplication start');
  flocalServer := createLocal;
  fRemoteClient := TRestHttpClient.Create(MASTER_IP, MASTER_PORT, createMasterModel);

  ILog.Log(sllEnter,'Slave startup - Timestamp sync');
  if not fRemoteClient.ServerTimestampSynchronize then
    writeMessage('ERROR! Please start the server(MasterServer)!');

  ILog.Log(sllEnter,'Slave startup - Servicedefine');
  fRemoteClient.ServiceDefine([IReplicationInterface], sicClientDriven, REPLICATION_TEST_CONTRACT);

  ILog.Log(sllEnter,'Slave startup - Resolve IReplicationInterface');
  fRemoteClient.Resolve(IReplicationInterface, fService); // Gives 'Invalid URI' from TReplicationServiceServer
  if fService = nil then
    writeMessage('ERROR! Could not create the service!');

  ILog.Log(sllEnter,'Slave startup - Create WebSocket');
  fMasterClient := TRestHttpClientWebsockets.Create(MASTER_IP, REPL_PORT, createReplicationModel);
  fMasterClient.Model.Owner := fMasterClient;
  fMasterClient.WebSockets.Settings.SetFullLog;
  fMasterClient.WebSocketsUpgrade('KeyForEncryption');

  ILog.Log(sllEnter,'Slave startup - SetUpSlaveServer');
  fSlave := createSlave;

  ILog.Log(sllEnter,'Slave startup - SlaveSyncStart');
  fSlave.RecordVersionSynchronizeSlaveStart(TOrmUser, fMasterClient, nil);
  createOK := true;
end;

var
  user, userx: TOrmUser;
  User2, User3: TOrmUser;
  masterStatus: TReplicationServiceError;
  i: integer;
  userID: RawUTF8;
begin
  ReportMemoryLeaksOnShutdown := true; //DebugHook <> 0;
  LogFamily := SQLite3Log.Family;
  LogFamily.Level := LOG_VERBOSE;
  LogFamily.PerThreadLog := ptIdentifiedInOnFile;
  LogFamily.EchoToConsole := LOG_VERBOSE;
  LogFamily.DestinationPath := 'K:\log\slave';

//** Create the Replicationclass which keeps the connection to the service.
  Replication := TReplicationClass.create;
  if not createOk then
    writeMessage('Could not create main class!');

//** Create some data on the master server.

  ILog.Log(sllEnter, 'Slave run - Delete existing users');
  for i := 0 to 3 do
  begin
    userID := format('Usr%d', [i]);
    Replication.fService.deleteUser(userID);
  end;
  writeLn('Check that all records in user-table at the server are removed');
  readLn;

  ILog.Log(sllEnter, 'Slave run - Add new user');
  user := TOrmUser.Create;
  for i := 0 to 3 do
  begin
    user.userID := format('usr%d', [i]);
    user.Name := format('Lars-%d', [i]);
    Replication.fService.addNewUser(user);
  end;
  freeAndNil(user);

  ILog.Log(sllEnter, 'Slave run - Check that all users exists at master db');
  for i := 0 to 3 do
  begin
    userID := format('usr%d', [i]);
    if Replication.fService.retrieveUser(userID, user) <> repSuccess then
      writeMessage(format('Could not retrieve user %s from master', [userID]));
  end;
  freeAndNil(user);
  sleep(100);

//** Data should now been replicated to the slave server.
  ILog.Log(sllEnter, 'Slave run - Check if user have been replicated');
  user3 := TOrmUser.Create;
  for i := 0 to 3 do
  begin
    userID := format('usr%d', [i]);
    if not Replication.fSlave.Retrieve('userID = ' + quotedStr(userID), user3) then
      writeMessage(format('User %s  has not been replicated', [userID]));
  end;

//** Now change data on master server
  user3.Name := 'Another User';
  if Replication.fService.updateUser(user3) <> repSuccess then
    writeMessage('user not updated');
  sleep(100);

//** Data should now been replicated to the slave server.
  user2 := TOrmUser.Create;
  userID := quotedStr(user3.UserID);
  Replication.fSlave.Retrieve('userID = ' + userID, user2);
  if (user2 = nil) or (user2.Name <> user3.Name) then
  begin
    writeMessage('user not replicated');
  end;

  readLn; //don't close the consol automatically as we want to check what's happening.
end.
