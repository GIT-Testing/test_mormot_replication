unit uSlaveServer;

interface
uses
  System.SysUtils
  , mormot.core.base
  , mormot.core.data
  , mormot.core.json
  , mormot.core.unicode
  , mormot.orm.core
  , mormot.soa.core
  , mormot.core.test
  , mormot.core.os
  , mormot.core.log
  , mormot.rest.core
  , mormot.rest.http.client
  , mormot.rest.client
  , mormot.core.collections
  , mormot.db.raw.sqlite3
  , mormot.db.raw.sqlite3.static
  , mormot.rest.sqlite3
  , mormot.rest.http.server
  , mormot.rest.server
  , mormot.net.ws.core
  , mormot.net.ws.client
  , mormot.net.ws.server
  , uOrmModel
;
function createLocal: TRestServerDB;
function CreateServer(const DBFileName: TFileName; const aModel: TOrmModel): TRestServerDB;
function CreateMasterModel: TOrmModel;
function CreateSlaveModel: TOrmModel;
function createSlave: TRestServerDB;

implementation
function CreateServer(const DBFileName: TFileName; const aModel: TOrmModel): TRestServerDB;
begin
  result := TRestServerDB.Create(aModel, DBFileName, false, '');
  result.Model.Owner := result;
  result.DB.Synchronous := smOff;
  result.DB.LockingMode := lmNormal;
  result.Server.CreateMissingTables;
end;

function CreateMasterModel: TOrmModel;
begin
  result := TOrmModel.Create([TOrmTableDeleted, TOrmUser], 'master');
end;

function CreateSlaveModel: TOrmModel;
begin
  result := TOrmModel.Create([TOrmTableDeleted, TOrmUser], 'slave');
end;

function CreateLocalModel: TOrmModel;
begin
  result := TOrmModel.Create([TOrmTableDeleted, TOrmUser], 'local');
end;

function createSlave: TRestServerDB;
var
  Model: TOrmModel;
  masterModel: TOrmModel;
  slave: TRestServerDB;
begin
  Model := CreateSlaveModel;
  result := nil;
  try
    slave := CreateServer('Slave.db', Model);
    result := slave;
  finally
  end;
end;

function createLocal: TRestServerDB;
var
  Model: TOrmModel;
  masterModel: TOrmModel;
  local: TRestServerDB;
begin
  Model := CreateLocalModel;
  result := nil;
  try
    local := CreateServer('Slave.db', Model);
    result := local;
  finally
  end;
end;

end.
