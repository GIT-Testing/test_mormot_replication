program MasterServer;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  mormot.core.base,
  mormot.core.data,
  mormot.core.json,
  mormot.core.unicode,
  mormot.orm.core,
  mormot.soa.core,
  mormot.core.test,
  mormot.core.os,
  mormot.core.log,
  mormot.rest.http.client,
  mormot.core.collections,
  mormot.db.raw.sqlite3,
  mormot.db.raw.sqlite3.static,
  mormot.rest.sqlite3,
  mormot.rest.memserver,
  mormot.rest.server,
  mormot.rest.http.server,
  uOrmModel in '..\uOrmModel.pas',
  uMasterServer in 'uMasterServer.pas',
  ReplicationTestInterfaces in '..\ReplicationTestInterfaces.pas',
  ReplicationTestService in 'ReplicationTestService.pas',
  uMasterServiceServer in 'uMasterServiceServer.pas';

var

  HttpMasterServer: TRestHttpServer;
  HttpReplServer: TRestHttpServer;
  LogFamily: TSynLogFamily;
//  MasterSrv: TRestServerDB;
  MasterReplSrv: TRestServerDB;
  MasterServiceServer: TReplicationServiceServer;
  fService: IReplicationInterface;
  model: TOrmModel;
  user: TOrmUser;
  id: TID;
  ILog: ISynLog;
begin
  ReportMemoryLeaksOnShutdown := true;//DebugHook <> 0;
  LogFamily := SQLite3Log.Family;
  LogFamily.Level := LOG_VERBOSE;
//  LogFamily.PerThreadLog := ptIdentifiedInOnFile;
  LogFamily.EchoToConsole := LOG_VERBOSE;
  LogFamily.DestinationPath := 'K:\log\master';

  ILog := TSynLog.enter('Masterserver logging',[]);
  ILog.Log(sllEnter,'Server startup - CreateMaster');

//  MasterSrv := CreateMaster;
  ILog.Log(sllEnter,'Server startup - CreateReplSrver');
  MasterReplSrv := uMasterServer.CreateReplServer;

  ILog.Log(sllEnter,'Server startup - TReplictionServiceServer');
  MasterServiceServer := TReplicationServiceServer.Create;

  try
  ILog.Log(sllEnter,'Server startup - Master-ServiceDefine');
//    MasterServiceServer.ServiceDefine(TReplicationService, [IReplicationInterface], sicClientDriven, REPLICATION_TEST_CONTRACT);

  ILog.Log(sllEnter,'Server startup - CreateHttpMaster');
    HttpMasterServer := TRestHttpServer.Create(MASTER_PORT, [MasterServiceServer], '+', HTTP_DEFAULT_MODE,4 );

  ILog.Log(sllEnter,'Server startup - CreateReplServer');
    HttpReplServer := TRestHttpServer.Create(REPL_PORT, [MasterReplSrv], '+', useBidirSocket,4 );
    HttpReplserver.WebSocketsEnable(MasterReplSrv, 'KeyForEncryption')^.SetFullLog;

  ILog.Log(sllEnter,'Server startup - SyncMasterStart');
    if MasterReplSrv.RecordVersionSynchronizeMasterStart(true) then

  ILog.Log(sllInfo, 'Master now waits for synchronize')
    else
  ILog.Log(sllError, 'Master can not start synchronize');

   writeln('Press return to exit....');
   readln
  finally
    HttpMasterServer.Free;
    MasterServiceServer.Free;
  end;
end.
