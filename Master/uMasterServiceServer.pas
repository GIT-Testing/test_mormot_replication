unit uMasterServiceServer;

interface
{$I mormot.defines.inc}
uses
  SysUtils
  , mormot.core.base
  , mormot.core.data
  , mormot.core.unicode
  , mormot.orm.core

  , mormot.rest.server
  , mormot.soa.core
  , mormot.soa.server

  , mormot.db.raw.sqlite3
  , mormot.db.raw.sqlite3.static
  , mormot.rest.sqlite3

  , mormot.rest.memserver
  , mormot.core.interfaces
  , ReplicationTestService
  , ReplicationTestInterfaces

;
type
//  TReplicationServiceServer = class(TRestServerFullMemory)
  TReplicationServiceServer = class(TRestServerDB)
  public
    constructor Create; overload;
  end;

implementation
uses
  uOrmModel
;
{
******************************** TServiceServer ********************************
}
constructor TReplicationServiceServer.Create;
var
  model: TOrmModel;
  Service: TReplicationService;
begin
  Model := createMasterModel;
  inherited Create(Model, false);
  Service := TReplicationService.Create;
  ServiceDefine(Service, [IReplicationInterface], REPLICATION_TEST_CONTRACT);
end;
end.
