unit ReplicationTestService;

interface
{$I mormot.defines.inc}
uses
  system.SysUtils
  , mormot.core.base
  , mormot.core.os
  , mormot.orm.base
  , mormot.orm.core
  , mormot.core.json
  , mormot.soa.server
  , mormot.db.raw.sqlite3
  , mormot.rest.sqlite3
  , mormot.db.raw.sqlite3.static
  , ReplicationTestInterfaces
  , uOrmModel
  , uMasterServer
;

type
  TReplicationService = class(TInjectableObjectRest, IReplicationInterface)
  private
    fRestServer: TRestServerDB;
  public
    function retrieveUserList(var pmvUserList: TOrmUserObjArray): TReplicationServiceError;
    function retrieveUser(const pmcUserID: RawUTF8; out pmoUser: TOrmUser): TReplicationServiceError;
    function addNewUser(var pmvOrmUser: TOrmUser): TReplicationServiceError;
    function updateUser(var pmvOrmUser: TOrmUser): TReplicationServiceError;
    function deleteUser(const pmcUserID: RawUTF8): TReplicationServiceError;
    function Test(out pmoMessage: RawUTF8): TReplicationServiceError;
    constructor create;
  end;
implementation
uses
  mormot.core.log
;
var
  ILog: ISynLog;

{ TReplicationService }

function TReplicationService.addNewUser(
  var pmvOrmUser: TOrmUser): TReplicationServiceError;
var
  id: TID;
begin
  ILog.Log(sllENTER,'addNewUser');
  id := fRestServer.Add(pmvOrmUser,true);
  if id > 0 then
    result := repSuccess
  else
    result := repReadFailure;
end;

constructor TReplicationService.create;
begin
  fRestServer := createMaster;
end;

function TReplicationService.deleteUser(
  const pmcUserID: RawUTF8): TReplicationServiceError;
begin
  ILog.Log(sllENTER,'deleteUser');
  if fRestServer.Delete(TOrmUser,'userID = '+ quotedStr(pmcUserID)) then
    result := repSuccess
  else
    result := repDeleteFailure;
end;

function TReplicationService.retrieveUser(
  const pmcUserID: RawUTF8; out pmoUser: TOrmUser): TReplicationServiceError;
begin
  ILog.Log(sllENTER,'retrieveUser');
  if fRestServer.Retrieve('userID = '+ quotedStr(pmcUserID), pmoUser) then
    result := repSuccess
  else
    result := repReadFailure;
 end;

function TReplicationService.retrieveUserList(
  var pmvUserList: TOrmUserObjArray): TReplicationServiceError;
begin
  ILog.Log(sllENTER,'retrieveUserList');
  if fRestServer.RetrieveListObjArray(pmvUserList,TOrmUser,'',[]) then
    result := repSuccess
  else
    result := repReadFailure;
end;

function TReplicationService.Test(
  out pmoMessage: RawUTF8): TReplicationServiceError;
begin
  pmoMessage := 'This is a test';
  result := repSuccess;
end;

function TReplicationService.updateUser(
  var pmvOrmUser: TOrmUser): TReplicationServiceError;
var
  usr: TOrmUser;
begin
  result := repUpdateFailure;
  ILog.Log(sllENTER, 'updateUser');
  usr := TOrmUser.create;
  if fRestServer.Retrieve('userID = ' + quotedStr(pmvOrmUser.userID), usr) then
  begin
    usr.Name := pmvOrmUser.Name;
    if fRestServer.upDate(usr, []) then
    begin
      pmvOrmUser := usr;
      result := repSuccess;
    end
  end;
end;

initialization
  ILog := TSynLog.create;
end.
