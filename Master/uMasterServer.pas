unit uMasterServer;

interface
uses
   System.SysUtils
   , mormot.orm.base
   , mormot.orm.core
   , mormot.rest.http.client
   , mormot.db.raw.sqlite3
   , mormot.db.raw.sqlite3.static
   , mormot.rest.sqlite3
   , mormot.rest.http.server
   , mormot.rest.server
   , mormot.net.ws.core
   , mormot.net.ws.client
   , mormot.net.ws.server
   , uOrmModel
;
function CreateReplServer: TRestServerDB;
function CreateMaster: TRestServerDB;
implementation
function CreateServer(const DBFileName: TFileName; const aModel: TOrmModel): TRestServerDB;
begin
  result := TRestServerDB.Create(aModel, DBFileName, false, '');
  result.Model.Owner := result;
  result.DB.Synchronous := smOff;
  result.DB.LockingMode := lmNormal;
  result.Server.CreateMissingTables;
end;

function setUpMasterServer(const aModel: TOrmModel): TRestServerDB;
begin
  result := CreateServer('Master.db', aModel);
//  result.Delete(TOrmUser, '');
end;

function CreateMaster: TRestServerDB;
var
  Model: TOrmModel;
begin
  Model := CreateMasterModel;
  result := SetUpMasterServer(Model);
end;

function CreateReplServer: TRestServerDB;
var
  Model: TOrmModel;
begin
  Model := createReplicationModel;
  result := SetUpMasterServer(Model);
end;

end.
