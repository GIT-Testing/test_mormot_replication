unit ReplicationTestInterfaces;

interface
uses
  mormot.core.interfaces
  , mormot.core.base
  , uOrmModel
;
const
  REPLICATION_TEST_CONTRACT = 'MASTER_SERVER';
type
  TReplicationServiceError = (repSuccess, repReadFailure, repUpdateFailure, repDeleteFailure);


  IReplicationInterface = interface(IInvokable)
    ['{7C879B56-0F31-41E8-993D-6664E48EC304}']
    function retrieveUserList(var pmvUserList: TOrmUserObjArray): TReplicationServiceError;
    function retrieveUser(const pmcUserID: RawUTF8; out pmoUser: TOrmUser): TReplicationServiceError;
    function addNewUser(var pmvOrmUser: TOrmUser): TReplicationServiceError;
    function updateUser(var pmvOrmUser: TOrmUser): TReplicationServiceError;
    function deleteUser(const pmcUserID: RawUTF8): TReplicationServiceError;
    function Test(out pmoMessage: RawUTF8): TReplicationServiceError;
  end;

implementation

initialization
  TInterfaceFactory.RegisterInterfaces([TypeInfo(IReplicationInterface)]);
end.
