# mORMot_Test_Replication
Test system to test the replication function of the mORMot2 library

1. Start the serverprogram first
2. Start the client(Slave_consol).
3. Create one or more user 
4. If the replication works as expected the user the database in the slave EXE-directory should be updated with the users created in the the master db.
5. When changes of the users name is updated those changes should be replicated to the slave database.

I'm working on a system that will be using the replication function to keep slave terminals updated from a central server.
The picture and/or DrawIO file gives an overview of how this simple system should work.

Master server IP: 127.0.0.1
Master server data port: 8206
Master server replication port: 8207 using websockets
